let port = 3000;
const express = require('express')
const app = express();
const bodyParser = require('body-parser');
const constants = require('./app/config/constants.config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
require('./app/routes')(app);

if(constants.test){
	port = 3001;
}

app.listen(port, function () {
  console.log('Listening on port : '+port)
});



module.exports = app;