# UserInbox

One Paragraph of project description goes here

## Getting Started

API Interfaces
```
Message API:
Post:
    Param: AppID, Title, MessageBody, MessageType, SSOID, TrueID, Priority (To be define)
PUT:
    Mark as READ (with date time)
DEL:
    Soft Delete
GET:
    Get detail of specified SSOID
    MessageList API:
GET:
    Get list of messages sort by Date Time (newest first, within 15 days)
    Ability to filter only READ/UNREAD message
    Ability to filter only specified AppID
```

### Prerequisites
Install the software and how to install them

```
npm , mysql
```

### Installing

run npm

```
npm install
```



## Running the tests

Explain run the automated tests for this system

### And coding style tests

```
npm test
```
