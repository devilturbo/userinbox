'use strict';

const assert = require('assert'),
      mocha = require('mocha'),
      coMocha = require('co-mocha'),
      chai = require('chai'),
      chaiHttp = require('chai-http'),
      should = chai.should(),
      version = '/v1',
      schedule = '/userinbox',
      service = 'notification'

let constants = require('../app/config/constants.config');
    constants.test = true;

let server = require('./../server');
chai.use(chaiHttp);
coMocha(mocha);

let auth = "59351c4b233eaa4928abdc0014fdc8faea744f1a47d32a4ee1d9a697"


let getCategory200 = {};    
let getMessageType200 = {};  
let postSuccess200Now = {};
let putMessage200     = {};
let putMessage400     = {};
let putMessage404     = {};
let deleteMessage404  = {};
let getListMessage200 = {};
let getListMessage400 = {};
let getListMessage404 = {};
let putBodyMessage200 = {};
let putBodyMessage400 = {};
let deleteBodyMessage204 = {};
let deleteBodyMessageByID204 = {};
let getBodyMessageType200  = {};
let getBodyCategoriesType200  = {};
let setBodySSOID200 = {};
let setBodySSOID404 = {};

let setSSOID    = "?ssoid=00090901SSO";
let setSSOID404 = "?ssoid=00090901SSOxxxSS";
let queryMessage404   = "?ssoid=00000001111119191919&isRead=9";
let queryMessage200   = "?ssoid=00090901SSO&isRead=1&appID=00001800&isPriority=1";
let setMessageID      = 1;
let setDeleteID       = 1;
let setPutMessage = {
    "isRead"    :true,
	"isPriority":true
}

let setPutMessage400 = {
    "isRead_false"    :true,
	"isPriority_false":true
}

let setData = {

    "appID"      : "00001800",
    "title"      : "test",
    "messageBody": "<h1>test</h1>",
    "categoryID" : 1,
    "msgTypeID"  : 3,
    "ssoid"      : "00090901SSO",
    "trueID"     : "00001911",
    "isPriority"   : true
}

let postBadData400 = {};
let setBadData = {
    "appID"      : "00001800",
    "title"      : "test",
    "messageBody": "<h1>test</h1>",
    "categoryID" : "a1",
    "msgTypeID"  : "a3A",
    "ssoid"      : "00090901SSO",
    "trueID"     : "00001911",
    "isPriority"   : true
}

let postMessage400 = {};
let setDataMissing = {
    "msgTypeID"  : 3,
    "ssoid"      : "00090901SSO",
    "trueID"     : "00001911",
    "isPriority"   : true
}

let postCategory404 = {};
let setDataCategory = {

    "appID"      : "00001800",
    "title"      : "test",
    "messageBody": "<h1>test</h1>",
    "categoryID" : 9,
    "msgTypeID"  : 3,
    "ssoid"      : "00090901SSO",
    "trueID"     : "00001911",
    "isPriority"   : true
}

let postMessage404 = {};
let setDataMessageType = {

    "appID"      : "00001800",
    "title"      : "test",
    "messageBody": "<h1>test</h1>",
    "categoryID" : 1,
    "msgTypeID"  : 99,
    "ssoid"      : "00090901SSO",
    "trueID"     : "00001911",
    "isPriority"   : true
}

suite('SET USERINBOX', () => {

    suite('SUCCESS 200 SET USERINBOX ', () => {
        before((done) => {
            chai.request(server)
              .post(version + schedule)
              .set('authorization', auth)
              .send(setData)
              .end((err, res) => {
                postSuccess200Now = res;
                done();
              })
        })

        test('should have status 201', () => {
            postSuccess200Now.should.have.status(201);
        });

        test('should be JSON', () => {
            postSuccess200Now.should.be.json;
        });

        test('should have code 201', () => {
            postSuccess200Now.body.status.code.should.equal(201)
        });
        test('should have message: "Create Success [ save appID : 00001800 ] "', () => {
            postSuccess200Now.body.status.message.should.equal("Create Success [save appID : 00001800]");
        });
        test('should have data', () => {
            postSuccess200Now.body.should.have.property('data');
            postSuccess200Now.body.data.should.have.property('ssoid');
            postSuccess200Now.body.data.should.have.property('categories_id');
            postSuccess200Now.body.data.should.have.property('messagetype_id');
            postSuccess200Now.body.data.should.have.property('title');
            postSuccess200Now.body.data.should.have.property('message');
            postSuccess200Now.body.data.should.have.property('trueid');
            postSuccess200Now.body.data.should.have.property('isPriority');
            postSuccess200Now.body.data.should.have.property('appid');
        })
    })

    suite('BAD DATA REQUEST 400 SET USERINBOX ', () => {
        before((done) => {
            chai.request(server)
              .post(version + schedule)
              .set('authorization', auth)
              .send(setBadData)
              .end((err, res) => {
                postBadData400 = res;
                done();
              })
        })

        test('should have status 400', () => {
            postBadData400.should.have.status(400);
        });

        test('should be JSON', () => {
            postBadData400.should.be.json;
        });

        test('should have code 400', () => {
            postBadData400.body.status.code.should.equal(400)
        });
        test('should have message: "Bad Request dataType invalid Or Missing Require field."', () => {
            postBadData400.body.status.message.should.equal("Bad Request dataType invalid Or Missing Require field.");
        });
       
    })

    suite('BAD DATA MISSING REQUEST 400 SET USERINBOX ', () => {
        before((done) => {
            chai.request(server)
              .post(version + schedule)
              .set('authorization', auth)
              .send(setDataMissing)
              .end((err, res) => {
                postMessage400 = res;
                done();
              })
        })

        test('should have status 400', () => {
            postMessage400.should.have.status(400);
        });

        test('should be JSON', () => {
            postMessage400.should.be.json;
        });

        test('should have code 400', () => {
            postMessage400.body.status.code.should.equal(400)
        });
        test('should have message: "Bad Request dataType invalid Or Missing Require field."', () => {
            postMessage400.body.status.message.should.equal("Bad Request dataType invalid Or Missing Require field.");
        });
       
    })
   
    suite('CATEGORY NOT FOUND 404 SET USERINBOX ', () => {
        before((done) => {
            chai.request(server)
              .post(version + schedule)
              .set('authorization', auth)
              .send(setDataCategory)
              .end((err, res) => {
                postCategory404 = res;
                done();
              })
        })

        test('should have status 404', () => {
            postCategory404.should.have.status(404);
        });

        test('should be JSON', () => {
            postCategory404.should.be.json;
        });

        test('should have code 404', () => {
            postCategory404.body.status.code.should.equal(404)
        });
        test('should have message: "categoryType Not Found."', () => {
            postCategory404.body.status.message.should.equal("categoryType Not Found.");
        });
       
    })

    suite('MESSAGETYPE NOT FOUND 404 SET USERINBOX ', () => {
        before((done) => {
            chai.request(server)
              .post(version + schedule)
              .set('authorization', auth)
              .send(setDataMessageType)
              .end((err, res) => {
                postMessage404 = res;
                done();
              })
        })

        test('should have status 404', () => {
            postMessage404.should.have.status(404);
        });

        test('should be JSON', () => {
            postMessage404.should.be.json;
        });

        test('should have code 404', () => {
            postMessage404.body.status.code.should.equal(404)
        });
        test('should have message: "messageType Not Found."', () => {
            postMessage404.body.status.message.should.equal("messageType Not Found.");
        });
       
    })

    suite('GET CATEGORY USERINBOX', () => {

        suite('SUCCESS 200 GET CATEGORY', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/categoriestype")
                  .set('authorization', auth)
                  .end((err, res) => {
                    getCategory200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getCategory200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getCategory200.should.be.json;
            });
    
            test('should have code 200', () => {
                getCategory200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getCategory200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getCategory200.body.should.have.property('data');
                getCategory200.body.data[0].should.have.property('id');
                getCategory200.body.data[0].should.have.property('categories');
                getCategory200.body.data[0].should.have.property('description');
            })
        })
    }); 
    
    let getBodyCategoryType200 = {}
    let getBodyCategoryType400 = {}
    let setDataCategoryType200    = {
        "categories" : "MVC",
        "description" : "MVP PLAYER"
    }
    let setDataCategoryType400    = {
      
        "description" : "MVP PLAYER"
    }
    suite('POST CATEGORY USERINBOX', () => {

        suite('SUCCESS 200 GET CATEGORY', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/categoriestype")
                  .set('authorization', auth)
                  .send(setDataCategoryType200)
                  .end((err, res) => {
                    getCategory200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getBodyCategoryType200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getBodyCategoryType200.should.be.json;
            });
    
            test('should have code 200', () => {
                getBodyCategoryType200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getBodyCategoryType200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getBodyCategoryType200.body.should.have.property('data');
                getBodyCategoryType200.body.data.should.have.property('categories');
                getBodyCategoryType200.body.data.should.have.property('description');
                getBodyCategoryType200.body.data.should.have.property('createddate');
                getBodyCategoryType200.body.data.should.have.property('updatedate');
            })
        })

        suite('BAD DATA 400 GET CATEGORY', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/categoriestype")
                  .set('authorization', auth)
                  .send(setDataCategoryType400)
                  .end((err, res) => {
                    getBodyCategoryType400 = res;
                    done();
                  })
            })
    
            test('should have status 400', () => {
                getBodyCategoryType400.should.have.status(400);
            });
    
            test('should be JSON', () => {
                getBodyCategoryType400.should.be.json;
            });
    
            test('should have code 400', () => {
                getBodyCategoryType400.body.status.code.should.equal(400)
            });
            test('should have message: "Bad Request dataType invalid Or Missing Require field."', () => {
                getBodyCategoryType400.body.status.message.should.equal("Bad Request dataType invalid Or Missing Require field.");
            });
           
        })
    });      

    suite('GET MESSAGE TYPE USERINBOX', () => {

        suite('SUCCESS 200 GET MESSAGE TYPE', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/messagetype")
                  .set('authorization', auth)
                  .end((err, res) => {
                    getMessageType200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getMessageType200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getMessageType200.should.be.json;
            });
    
            test('should have code 200', () => {
                getMessageType200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getMessageType200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getMessageType200.body.should.have.property('data');
                getMessageType200.body.data[0].should.have.property('id');
                getMessageType200.body.data[0].should.have.property('message_type');

            })
        })
    });        

    suite('PUT MESSAGE USERINBOX', () => {

        suite('SUCCESS 200 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .put(version + schedule + "/" + setMessageID)
                  .set('authorization', auth)
                  .end((err, res) => {
                    putMessage200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                putMessage200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                putMessage200.should.be.json;
            });
    
            test('should have code 200', () => {
                putMessage200.body.status.code.should.equal(200)
            });
            test('should have message: "Success [ Update message ID :'+setMessageID+'] "', () => {
              
                putMessage200.body.status.message.should.equal("Success [Update message ID :"+setMessageID+"]");
            });
            test('should have data', () => {
                putMessage200.body.should.have.property('data');
                putMessage200.body.data.should.have.property('appID');
                putMessage200.body.data.should.have.property('title');
                putMessage200.body.data.should.have.property('messageBody');
                putMessage200.body.data.should.have.property('categoryID');
                putMessage200.body.data.should.have.property('msgTypeID');
                putMessage200.body.data.should.have.property('ssoid');
                putMessage200.body.data.should.have.property('trueid');
                putMessage200.body.data.should.have.property('isPriority');
                putMessage200.body.data.should.have.property('isRead');
                putMessage200.body.data.should.have.property('updatedDate');
                putMessage200.body.data.should.have.property('createdDate');
            })
        })
    });

    suite('PUT MESSAGE AND SET BODY USERINBOX', () => {

        suite('SUCCESS 200 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .put(version + schedule + "/" + setMessageID)
                  .set('authorization', auth)
                  .send(setPutMessage)
                  .end((err, res) => {
                    putBodyMessage200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                putBodyMessage200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                putBodyMessage200.should.be.json;
            });
    
            test('should have code 200', () => {
                putBodyMessage200.body.status.code.should.equal(200)
            });
            test('should have message: "Success [Update message ID :'+setMessageID+']"', () => {
                putBodyMessage200.body.status.message.should.equal("Success [Update message ID :"+setMessageID+"]");
            });
            test('should have data', () => {
                putBodyMessage200.body.should.have.property('data');
                putBodyMessage200.body.data.should.have.property('appID');
                putBodyMessage200.body.data.should.have.property('title');
                putBodyMessage200.body.data.should.have.property('messageBody');
                putBodyMessage200.body.data.should.have.property('categoryID');
                putBodyMessage200.body.data.should.have.property('msgTypeID');
                putBodyMessage200.body.data.should.have.property('ssoid');
                putBodyMessage200.body.data.should.have.property('trueid');
                putBodyMessage200.body.data.should.have.property('isPriority');
                putBodyMessage200.body.data.should.have.property('isRead');
                putBodyMessage200.body.data.should.have.property('updatedDate');
                putBodyMessage200.body.data.should.have.property('createdDate');
            })
        })
    });

    suite('PUT MESSAGE PUT BODY USERINBOX', () => {

        suite('BAD REQUEST PUT BODY 400 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .put(version + schedule + '/' + setMessageID)
                  .set('authorization', auth)
                  .send(setPutMessage400)
                  .end((err, res) => {
                    putBodyMessage400 = res;
                    done();
                  })
            })
    
            test('should have status 400', () => {
                putBodyMessage400.should.have.status(400);
            });
    
            test('should be JSON', () => {
                putBodyMessage400.should.be.json;
            });
    
            test('should have code 400', () => {
                putBodyMessage400.body.status.code.should.equal(400)
            });
            test('should have message: "Bad Request dataType invalid Or Missing Require field."', () => {
            
                putBodyMessage400.body.status.message.should.equal("Bad Request dataType invalid Or Missing Require field.");
            });
           
        })
    });

    suite('PUT MESSAGE USERINBOX', () => {

        suite('BAD REQUEST 400 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .put(version + schedule)
                  .set('authorization', auth)
                  .end((err, res) => {
                    putMessage400 = res;
                    done();
                  })
            })
    
            test('should have status 400', () => {
                putMessage400.should.have.status(400);
            });
    
            test('should be JSON', () => {
                putMessage400.should.be.json;
            });
    
            test('should have code 400', () => {
                putMessage400.body.status.code.should.equal(400)
            });
            test('should have message: "User must provide messageId"', () => {
                putMessage400.body.status.message.should.equal("User must provide messageId");
            });
           
        })
    });



    suite('PUT MESSAGE USERINBOX', () => {

        suite('DATA NOT FOUND 404 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .put(version + schedule +'/1040')
                  .set('authorization', auth)
                  .end((err, res) => {
                    putMessage404 = res;
                    done();
                  })
            })
    
            test('should have status 404', () => {
                putMessage404.should.have.status(404);
            });
    
            test('should be JSON', () => {
                putMessage404.should.be.json;
            });
    
            test('should have code 404', () => {
                putMessage404.body.status.code.should.equal(404)
            });
            test('should have message: "MessageId not found [1040] "', () => {
                putMessage404.body.status.message.should.equal("MessageId not found [1040]");
            });
           
        })
    });

    suite('GET LIST MESSAGE USERINBOX', () => {

        suite('SUCCESS 200 GET MESSAGE', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + queryMessage200)
                  .set('authorization', auth)
                  .end((err, res) => {
                    getListMessage200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getListMessage200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getListMessage200.should.be.json;
            });
    
            test('should have code 200', () => {
                getListMessage200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getListMessage200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getListMessage200.body.should.have.property('data');
                getListMessage200.body.data[0].should.have.property('messageID');
                getListMessage200.body.data[0].should.have.property('ssoid');
                getListMessage200.body.data[0].should.have.property('categories_id');
                getListMessage200.body.data[0].should.have.property('messagetype_id');
                getListMessage200.body.data[0].should.have.property('title');
                getListMessage200.body.data[0].should.have.property('message');
                getListMessage200.body.data[0].should.have.property('isRead');
                getListMessage200.body.data[0].should.have.property('isDelete');
                getListMessage200.body.data[0].should.have.property('trueid');
                getListMessage200.body.data[0].should.have.property('isPriority');
                getListMessage200.body.data[0].should.have.property('updatedate');
                getListMessage200.body.data[0].should.have.property('createddate');
                getListMessage200.body.data[0].should.have.property('appid');
               
            })
        })
    });
  

    suite('GET MESSAGE BY SSOID USERINBOX', () => {

        suite('SUCCESS 200 GET MESSAGE', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + setSSOID)
                  .set('authorization', auth)
                  .end((err, res) => {
                    setBodySSOID200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                setBodySSOID200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                setBodySSOID200.should.be.json;
            });
    
            test('should have code 200', () => {
                setBodySSOID200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                setBodySSOID200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                setBodySSOID200.body.should.have.property('data');
                setBodySSOID200.body.data.should.have.property('messageID');
                setBodySSOID200.body.data.should.have.property('appID');
                setBodySSOID200.body.data.should.have.property('title');
                setBodySSOID200.body.data.should.have.property('messageBody');
                setBodySSOID200.body.data.should.have.property('categoryID');
                setBodySSOID200.body.data.should.have.property('msgTypeID');
                setBodySSOID200.body.data.should.have.property('ssoid');
                setBodySSOID200.body.data.should.have.property('trueid');
                setBodySSOID200.body.data.should.have.property('isPriority');
                setBodySSOID200.body.data.should.have.property('isRead');
                setBodySSOID200.body.data.should.have.property('updatedDate');
                setBodySSOID200.body.data.should.have.property('createdDate');
               
            })
        })

        suite('NOT FOUND 404 MESSAGE', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + setSSOID404)
                  .set('authorization', auth)
                  .end((err, res) => {
                    setBodySSOID404 = res;
                    done();
                  })
            })
    
            test('should have status 404', () => {
                setBodySSOID404.should.have.status(404);
            });
    
            test('should be JSON', () => {
                setBodySSOID404.should.be.json;
            });
    
            test('should have code 404', () => {
                setBodySSOID404.body.status.code.should.equal(404)
            });
            test('should have message: "Data Not Found."', () => {
                setBodySSOID404.body.status.message.should.equal("Data Not Found.");
            });
           
        })
    });

    
    
    suite('GET LIST MESSAGE USERINBOX', () => {

        suite('BAD REQUEST 400 MESSAGE', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule)
                  .set('authorization', auth)
                  .end((err, res) => {
                    getListMessage400 = res;
                    done();
                  })
            })
    
            test('should have status 400', () => {
                getListMessage400.should.have.status(400);
            });
    
            test('should be JSON', () => {
                getListMessage400.should.be.json;
            });
    
            test('should have code 400', () => {
                getListMessage400.body.status.code.should.equal(400)
            });
            test('should have message: "[isRead] Or [appID] Or [SSOID] must be not null"', () => {
                getListMessage400.body.status.message.should.equal("[isRead] Or [appID] Or [SSOID] must be not null");
            });
           
        })
    });

    suite('GET LIST MESSAGE USERINBOX', () => {

        suite('DATA NOT FOUND 404 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + queryMessage404)
                  .set('authorization', auth)
                  .end((err, res) => {
                    getListMessage404 = res;
                    done();
                  })
            })
    
            test('should have status 404', () => {
                getListMessage404.should.have.status(404);
            });
    
            test('should be JSON', () => {
                getListMessage404.should.be.json;
            });
    
            test('should have code 404', () => {
                getListMessage404.body.status.code.should.equal(404)
            });
            test('should have message: "Data Not Found."', () => {
                getListMessage404.body.status.message.should.equal("Data Not Found.");
            });
           
        })
    });

    

    

    suite('CATEGORY && MESSAGETYPE  USERINBOX', () => {

        suite('MESSAGETYPE SUCCESS 200 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/messagetype")
                  .set('authorization', auth)
                  .end((err, res) => {
                    getBodyMessageType200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getBodyMessageType200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getBodyMessageType200.should.be.json;
            });
    
            test('should have code 200', () => {
                getBodyMessageType200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getBodyMessageType200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getBodyMessageType200.body.should.have.property('data');
                getBodyMessageType200.body.data[0].should.have.property('id');
                getBodyMessageType200.body.data[0].should.have.property('message_type');
            })
        })

        suite('CATEGORY SUCCESS 200 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .get(version + schedule + "/categoriestype")
                  .set('authorization', auth)
                  .end((err, res) => {
                    getBodyCategoriesType200 = res;
                    done();
                  })
            })
    
            test('should have status 200', () => {
                getBodyCategoriesType200.should.have.status(200);
            });
    
            test('should be JSON', () => {
                getBodyCategoriesType200.should.be.json;
            });
    
            test('should have code 200', () => {
                getBodyCategoriesType200.body.status.code.should.equal(200)
            });
            test('should have message: "Success"', () => {
                getBodyCategoriesType200.body.status.message.should.equal("Success");
            });
            test('should have data', () => {
                getBodyCategoriesType200.body.should.have.property('data');
                getBodyCategoriesType200.body.data[0].should.have.property('id');
                getBodyCategoriesType200.body.data[0].should.have.property('categories');
                getBodyCategoriesType200.body.data[0].should.have.property('description');
            })
        })


    });

    suite('DELETE SET USERINBOX', () => {

        // suite('DELETE 204 SET USERINBOX', () => {
        //     before((done) => {
        //         chai.request(server)
        //           .delete(version + schedule)
        //           .set('authorization', auth)
        //           .end((err, res) => {
        //             deleteBodyMessage204 = res;
        //             done();
        //           })
        //     })
    
        //     test('should have status 204', () => {
        //         deleteBodyMessage204.should.have.status(204);
        //     });
        // });

        // suite('DELETE 204 ByID  SET USERINBOX', () => {
        //     before((done) => {
        //         chai.request(server)
        //           .delete(version + schedule + '/' + setDeleteID)
        //           .set('authorization', auth)
        //           .end((err, res) => {
        //             deleteBodyMessage204 = res;
        //             done();
        //           })
        //     })
    
        //     test('should have status 204', () => {
        //         deleteBodyMessage204.should.have.status(204);
        //     });
        // });

        

        suite('DELETE 404 SET USERINBOX', () => {
            before((done) => {
                chai.request(server)
                  .delete(version + schedule)
                  .set('authorization', auth)
                  .end((err, res) => {
                    deleteMessage404 = res;
                    done();
                  })
            })
    
            test('should have status 404', () => {
                deleteMessage404.should.have.status(404);
            });
    
            test('should be JSON', () => {
                deleteMessage404.should.be.json;
            });
    
            test('should have code 404', () => {
                deleteMessage404.body.status.code.should.equal(404)
            });
            test('should have message: "Message Not Found."', () => {
                deleteMessage404.body.status.message.should.equal("Message Not Found.");
            });
           
        });

    });
     
    
    
});

     