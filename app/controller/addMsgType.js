const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const validate = require('simple-json-validate');
const requestError = require('../config/request.config');
const patternHelper = require('../config/patternHelper');
const constants = require('../config/constants.config');

router.post('/messagetype', function (req, res) {
    var json = req.body;
    let validated = new validate(patternHelper.validateMessageType());
    let checkValidate = validated.check('addMessageType', json);
  
    if(checkValidate.isValid == false){
      requestError.getRequestErr(res,{status:"400_datatype_require"});
    }else{

      json.table = constants.dbMessageType
      users.addTypeUserInbox(json).then(function(result){
        requestError.getRequestErr(res,result);
      }).catch(function(err){
        requestError.getRequestErr(res,err);
      });
    }
});
/**
 * 
 * @param {1. "checkValidate" formmat form patternHelper.validateAddUserinbox check DataType and Data Require} json 
 * @param {2. if validate data = true : Build data in function addTypeUserInbox} json 
 */

  module.exports = router;