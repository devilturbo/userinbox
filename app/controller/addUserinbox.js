const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const validate = require('simple-json-validate');
const requestError = require('../config/request.config');
const patternHelper = require('../config/patternHelper');

router.post('/', function (req, res) {
    var json = req.body;
    
    let validated = new validate(patternHelper.validateAddUserinbox());
    let checkValidate = validated.check('addMessageValidate', json);
  
    if(checkValidate.isValid == false){
      requestError.getRequestErr(res,{status:"400_datatype_require"});
    }else{
  
      users.addMessage(json).then(function(result){
        requestError.getRequestErr(res,result);
      }).catch(function(err){
        requestError.getRequestErr(res,err);
      });
    }
  });
/**
 * 
 * @param {1. "checkValidate" formmat form patternHelper.validateAddUserinbox check DataType and Data Require} json 
 * @param {2. if validate data = true : Build data in function AddMessage} json 
 */

  module.exports = router;