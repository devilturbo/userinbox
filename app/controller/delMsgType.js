const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');
const constants = require('../config/constants.config');
const empty = require('is-empty');

router.delete(['/messagetype/:id', '/'], function (req, res) {
      var id = req.params.id || req.query.id;
      json = {
         id    : id,
         table : constants.dbMessageType
      }
      if(empty(id))
      {
        requestError.getRequestErr(res,{status:"400_userinbox_require"});
      }
      else
      {
        users.deleteTypeUserInbox(json).then(function(result){
          requestError.getRequestErr(res,result);
        }).catch(function(err){
          requestError.getRequestErr(res,err);
        });
      }
  });



  module.exports = router;