const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const validate = require('simple-json-validate');
const requestError = require('../config/request.config');
const patternHelper = require('../config/patternHelper');
const empty = require('is-empty');


router.put(['/:messageID', '/'], function (req, res) {
    var json = req.body;
    var messageID = req.params.messageID || undefined;
    let validated = new validate(patternHelper.validatemessageID());
    let checkValidate = validated.check('updateMessageValidate', {messageID:messageID});
    var obj = {}  
    if(!empty(json.isRead))
      obj.isRead = json.isRead
    
    if(!empty(json.isPriority))
      obj.isPriority = json.isPriority

    if(checkValidate.isValid == false ){
        requestError.getRequestErr(res,{status:"400_put_error"});
    }else if(!empty(json) && empty(obj)){
       requestError.getRequestErr(res,{status:"400_datatype_require"});
    }else{
      users.updateMessage(messageID,obj).then(function(result){
        requestError.getRequestErr(res,result);
      }).catch(function(err){
        requestError.getRequestErr(res,err);
      });
    }
    
    
  });

/**
* @param {1.  Check messageID} json 
* @param {2.  Check Json Body isRead and isPriority} json 
* @param {3.  validate true build in function updateMessage} json 
*/

  module.exports = router;