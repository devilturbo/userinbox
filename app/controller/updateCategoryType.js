const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');
const empty = require('is-empty');
const constants = require('../config/constants.config');


router.put(['/categoriestype/:id', '/categories'], function (req, res) {
    var json = req.body;
    var id = req.params.id || req.query.id;;
    if(!empty(id)){
       var objCon = {
           id : id,
           table : constants.dbCategoriesType
       }

       if(empty(json.categories)){
          requestError.getRequestErr(res,{status:"400_datatype_require"});
       }else{
          users.updateTypeUserInbox(objCon,json).then(function(result){
            requestError.getRequestErr(res,result);
          }).catch(function(err){
            requestError.getRequestErr(res,err);
          });
       }

    }else{
       requestError.getRequestErr(res,{status:"400_putID_error"});
    }

});

/**
* @param {1.  Check ID} json 
* @param {2.  Check Json Body messageType} json 
* @param {3.  objCon Json set id And Table name} json 
* @param {4.  validate true build in function updateMessage} json 
*/

  module.exports = router;