const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');

router.get('/messagetype', function (req, res) {
  users.findMessagetype().then(function(result){
    requestError.getRequestErr(res,result);
  }).catch(function(err){
    requestError.getRequestErr(res,err);
  });
});
/**
 * @param {1.  Build data in function findMessagetype} json 
 */
module.exports = router;