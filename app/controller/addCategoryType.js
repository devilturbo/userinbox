const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const validate = require('simple-json-validate');
const requestError = require('../config/request.config');
const patternHelper = require('../config/patternHelper');
const constants = require('../config/constants.config');

router.post('/categoriestype', function (req, res) {
    var json = req.body;
    console.log(json)
    let validated = new validate(patternHelper.validateCategoriesType());
    let checkValidate = validated.check('addCategoriesType', json);
  
    if(checkValidate.isValid == false){
      requestError.getRequestErr(res,{status:"400_datatype_require"});
    }else{
      
      json.table = constants.dbCategoriesType,
      delete json.isValid;
      users.addTypeUserInbox(json).then(function(result){
        requestError.getRequestErr(res,result);
      }).catch(function(err){
        requestError.getRequestErr(res,err);
      });
    }
});
/**
 * 
 * @param {1. "checkValidate" formmat form patternHelper.validateAddUserinbox check DataType and Data Require} json 
 * @param {2. if validate data = true : Build data in function addTypeUserInbox} json 
 */

  module.exports = router;