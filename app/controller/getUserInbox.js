const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');
const empty = require('is-empty');

router.get('/', function (req, res) {
    var json = req.query;
    


    if(empty(json.appID) && empty(json.isRead) && empty(json.ssoid) && empty(json.isPriority)){
        requestError.getRequestErr(res,{status:"400_list_error"});
    }else if(json.ssoid && empty(json.appID) && empty(json.isRead) && empty(json.isPriority)){
        users.getMessageBySSOID(json.ssoid).then(function(result) {
            requestError.getRequestErr(res,result);
          }).catch(function(err){
              requestError.getRequestErr(res,err);
          });
    }else{
        users.getListMessage(json).then(function(result) {
            requestError.getRequestErr(res,result);
          }).catch(function(err){
              requestError.getRequestErr(res,err);
          });
    }
});

/**
 * @param { 1. ssoid,isRead,appid,isPriority Optional Query string Require 1 value} json 
 * @param { 2. if ssoid only Return DATA BY SSOID 1 Record} json 
 * @param { 3. if full query string return getListmessage} json 
 */

module.exports = router;