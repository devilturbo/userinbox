const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');

router.get('/categoriestype', function (req, res) {
  users.findCategories().then(function(result){
    requestError.getRequestErr(res,result);
  }).catch(function(err){
    requestError.getRequestErr(res,err);
  });
});


/**
 * @param {1.  Build data in function findCategories} json 
 */

module.exports = router;