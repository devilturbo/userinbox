const express = require('express')
const router = express.Router()
const users = require('../model/userinbox');
const requestError = require('../config/request.config');

router.delete(['/:messageID', '/'], function (req, res) {
      var messageID = req.params.messageID || req.query.messageID;
     
      users.delMessage(messageID).then(function(result){
        requestError.getRequestErr(res,result);
      }).catch(function(err){
        requestError.getRequestErr(res,err);
      });
  });

/**
 * @param {1. if messageID not null Delete data none condition data - 15 day , if messageID is null Delete data from date <= 15} json 
 */

  module.exports = router;