const empty = require('is-empty');
'use strict';


 var   response_code = {
        "200_success" : {
            "header_code" : 200,
            "code" : "Success",
            "message": "Success"
        }, "201_created" : {
            "header_code" : 201,
            "code" : "Created",
            "message": "Create Success"
        }, "204_delete" : {
            "header_code" : 204,
            "code" : 204,
            "message": "NO CONTENT"
        },"400_put_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "User must provide messageId"
        },"400_putID_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "User must provide ID"
        },"400_del_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "Bad Request messageId must be not null"
        },"400_userinbox_require" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "Bad Request Missing Require field."
        },"400_datatype_require" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "Bad Request dataType invalid Or Missing Require field."
        },"400_category_require" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "categoryType Require field."
        },"400_read_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "Read Message and updatedDate Error"
        },"400_sso_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "Bad Request SSOID must be not null"
        },"400_list_error" : {
            "header_code" : 400,
            "code" : 400,
            "message" : "[isRead] Or [appID] Or [SSOID] must be not null"
        },"404_sso_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "SSOID Not Found."
        },"404_messagetype_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "messageType Not Found."
        },"404_put_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "MessageId not found"
        },"404_del_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "Message Not Found."
        },"404_list_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "Data Not Found."
        },"404_read_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "Read Message Not Found."
        },"404_category_error": {
            "header_code" : 404,
            "code" : 404,
            "message" : "categoryType Not Found."
        },"500_created_error": {
            "header_code" : 500,
            "code" : 500,
            "message" : "Cannot created data"
        },"500_db_error": {
            "header_code" : 500,
            "code" : 500,
            "message" : "Cannot connect DB"
        }
    }



/**
* @param { return herder HTTP and Message} json 
*/
var getRequestErr = function (res,data){
    var obj_message = {};
    obj_message.code = response_code[data.status].header_code
    obj_message.message = response_code[data.status].message
    if(data.message)
      obj_message.message = response_code[data.status].message+' ['+data.message+']'

    var setMessage = {status:obj_message};
   
    if(data.resultJson){
        if(data.page)
        setMessage = {status:obj_message,page:data.page,data:data.resultJson};
        else
         setMessage = {status:obj_message,data:data.resultJson};
    }
 
    return  res.status(response_code[data.status].header_code).json(setMessage);
}

module.exports = {
    getRequestErr
    
}