
/**
* @param {set insertData UserInbox} json 
*/
var jmInsert = function(){
    return pattern = {
        ssoid          : 'ssoid',
        categories_id  : 'categoryID',
        messagetype_id : 'msgTypeID',
        title          : 'title',
        message        : 'messageBody',
        trueid         : 'trueID',
        isPriority     : function(result){
                          if (result.isPriority == 1){
                            return true;
                          } else {
                            return false;
                          }
                        },  
        appid          : 'appID'
      }
}

/**
* @param {set listMessage UserInbox} json 
*/
var jmSetdata = function(){
    return pattern = {
        messageID   : 'messageID',
        appID       : 'appid',
        title       : 'title',
        messageBody : 'message',
        categoryID  : 'categories_id',
        msgTypeID   : 'messagetype_id',
        ssoid       : 'ssoid',
        trueid      : 'trueid',
        isPriority  : function(result){
                      if (result.isPriority == 1){
                        return true;
                      } else {
                        return false;
                      }
                    },  
        isRead      :  function(result){
                        if (result.isRead == 1){
                            return true;
                        } else {
                            return false;
                        }
                    }, 
        updatedDate : 'updatedate',
        createdDate : 'createddate'
      }
}



 
/**
* @param { validate data saveUserInbox} json 
*/
var validateAddUserinbox = function(){
    return pattern = {
        "addMessageValidate": {
            "categoryID": "required|number",
            "msgTypeID": "required|number",
            "messageBody": "required",
            "appID": "required",
            "title": "required",
            "ssoid": "required",
            "trueID": "required",
            "isPriority": "required"
        }
      };
}

/**
* @param { validate messageID updateUserInbox} json 
*/
var validatemessageID =  function(){
    return pattern = {
        "updateMessageValidate": {
            "messageID": "required|number"
        }
      };
}

var validateMessageType =  function(){
    return pattern = {
        "addMessageType": {
            "messageType": "required|max_length[100]|min_length[1]"
        }
      };
}

var validateCategoriesType =  function(){
    return pattern = {
        "addCategoriesType": {
            "categories"  : "required|max_length[150]|min_length[1]",
            "description" : "max_length[255]"
        }
      };
}

module.exports = {
    validatemessageID,
    validateAddUserinbox,
    jmInsert,
    jmSetdata,
    validateMessageType,
    validateCategoriesType
}