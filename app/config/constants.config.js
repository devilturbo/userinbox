'use strict';
let test        = false
let dbMessageType    = 'messagetype_userinbox'
let dbCategoriesType = 'categories_userinbox'
let dbUserInbox      = 'message_userinbox'
module.exports = {
    dbMessageType,
    dbCategoriesType,
    dbUserInbox,
    test
}