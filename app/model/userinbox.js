const db = require('../config/condb');
const empty = require('is-empty');
const moment = require('moment');
const requestError = require('../config/request.config');
const patternHelper = require('../config/patternHelper');
const JM = require('json-mapper');
const constants = require('../config/constants.config');

/**
 * 
 * @param {find Category Type Binding categoryID on saveUserInbox (ex. 1 = promotion , 2 = Maintenance)} json 
 */
var findCategories = async function() {
  try{
    let category = await db.knex.select('id','categories','description').table(constants.dbCategoriesType).where({isDelete:0})
    return Promise.resolve({
      status: "200_success",
      resultJson : category
    })
  }catch(e){
    return Promise.reject({
      status:  "500_db_error"
    })
  }
    
};

/**
 * 
 * @param {find Message Type Binding msgTypeID on saveUserInbox (ex. 1 = TEXT , 2 = HTML)} json 
 */
var findMessagetype = async function () {
  try{
    let messageType = await db.knex.select('id','message_type').table(constants.dbMessageType).where({isDelete:0})
    return Promise.resolve({
      status:  "200_success",
      resultJson : messageType
    })
  }catch(e){
    return Promise.reject({
      status:  "500_db_error"
    })
  }
};

/**
 * 
 * @param {json.message_type = string , json.table = tableName (cateType,msgType)} json 
 * @param {delete isValid , table before Return)} json 
 */

var addTypeUserInbox = async function (json){
  try{
       setTableUserInbox = json.table
       json.createddate   = moment().format('YYYY-MM-DD HH:mm:ss')
       json.updatedate   = moment().format('YYYY-MM-DD HH:mm:ss')
      console.log(json)
       delete json.table
       let data =  await db.knex(setTableUserInbox).insert(json)
       if(data){
          delete json.isValid
          delete json.table
          return Promise.resolve({
            status  :  "201_created",
            resultJson : json
        })
       }else{
        return Promise.reject({
          status:  "400_userinbox_require"
        })
       }
  }catch(e){
  console.log(e)
    return Promise.reject({
      status:  "500_created_error"
    })
  }
}

var updateTypeUserInbox = async function (objCon,json){
  try{
       json.updatedate = moment().format('YYYY-MM-DD HH:mm:ss')
       let data =  await db.knex(objCon.table).where({id: objCon.id,isDelete : 0}).update(json)
        if(empty(data)){
            return Promise.reject({
              status:  "404_list_error"
            })
        }else{
          let result =  await db.knex.select( '*',
              db.knex.raw("if(isDelete = 1, 'true', 'false') as isDelete"),
              db.knex.raw('DATE_FORMAT(createddate, "%Y-%m-%d %H:%i:%s") as createddate'),
              db.knex.raw('DATE_FORMAT(updatedate, "%Y-%m-%d %H:%i:%s") as updatedate'))
              .from(objCon.table).where({id:objCon.id}).limit(1) 

          return Promise.resolve({
          status  :  "200_success",
          resultJson : result
          })
        }

  }catch(e){
    console.log(e)
    return Promise.reject({
      status:  "500_db_error"
    })
  }
}



/**
 * 
 * @param {json.table = tableName (cateType,msgType) , json.id = id delete , update isDelete=1} json 
 */

var deleteTypeUserInbox = async function (json){
  try{
    let result =  updateMessage = await db.knex(json.table).where({id: json.id,isDelete : 0}).update({
      isDelete:1,
      updatedate : moment().format('YYYY-MM-DD HH:mm:ss')
    })
    if(empty(result)){
      return Promise.reject({
        status  :  "404_del_error"
      })
    }else{
      return Promise.resolve({
        status  :  "204_delete"
      })
    }
  }catch(e){
    return Promise.reject({
      status:  "500_db_error"
    })
  }
}

/**
 * 
 * @param { 1. ssoid,isRead,appid,isPriority Optional Query string Require 1 value} json 
 * @param { 2. pagination page = ConditonPage Query , pagesize = data-limit , defaultPage = return pagePresent , recordTotal = Total record from Query} json 
 * @param { 3. getResult = find total record from userinbox in Condition} json
 * @param { 4. getResult > 0 : find UserInbox add Condition Pagination } json 
 * @param { 5. return Data  isRead,isDelete and isPriority return true , false} json 
 */
var getListMessage = async function (json) {
  try{
    var jsonQuery = {}
    jsonQuery.isDelete = 0;
    json.page               = json.page || 0
    json.pageSize           = json.pageSize   || 5
    json.defaultPage        = json.page || 1
    json.recordTotal        = 0

    if(json.ssoid)
       jsonQuery.ssoid = json.ssoid
    if(json.isRead)
       jsonQuery.isRead = json.isRead
    if(json.appID)
       jsonQuery.appid = json.appID 
    if(json.isPriority)
       jsonQuery.isPriority = json.isPriority  

    
    let ssoResult = null
    let getResult = await db.knex(constants.dbUserInbox).count('messageID as record').where(jsonQuery)  
    if(getResult[0].record > 0){
      json.pageTotal      =  Math.ceil(( getResult[0].record / json.pageSize ))
      json.page           =  (((json.page > 0 ?(json.page-1) :0)) * json.pageSize)
      json.recordTotal    =  getResult[0].record

        ssoResult  =   await db.knex.select('*',
        db.knex.raw("if(isRead   = 0, 'true', 'false') as isRead"),
        db.knex.raw("if(isDelete = 1, 'true', 'false') as isDelete"),
        db.knex.raw("if(isPriority <> '', if(isPriority = 1,'true','fasle'), isPriority) as isPriority"),
        db.knex.raw('DATE_FORMAT(createddate, "%Y-%m-%d %H:%i:%s") as createddate'),
        db.knex.raw('DATE_FORMAT(updatedate, "%Y-%m-%d %H:%i:%s") as updatedate'))
        .table(constants.dbUserInbox)
        .where(jsonQuery)
        .orderBy('createddate', 'desc')
        .limit(json.pageSize)
        .offset(json.page);
    }

    
   if(empty(ssoResult)){
    
    return Promise.reject({
      status:  "404_list_error"
    })
   }else{
  
    return Promise.resolve({
      status     :  "200_success",
      page       : {pageSize : Number(json.pageSize) , page : Number(json.defaultPage) , pageTotal : Number(json.pageTotal) , recordTotal : getResult[0].record},
      resultJson : ssoResult
    
    })
   }
    
  }catch(e){
   console.log(e)
    return Promise.reject({
      status:  "500_db_error"
    })
  }
};


/**
 * 
 * @param {addMessage json = save Content (appID,title,messageBody,categoryID,msgTypeID,ssoid,trueID,isPriority)} json 
 * @param {Prepair Data 1. find Category from categoryID  2. find messageType from msgTypeID} json 
 * @param {saveMessage : categoryID and messageType is not null => save to db} json 
 */
var addMessage = async function (json) {
  try {
      let checkCategories  =  await  db.knex.select('id').from(constants.dbCategoriesType).where({'id':json.categoryID,isDelete:0}).limit(1);
      let checkMassage  =  await  db.knex.select('id').from(constants.dbMessageType).where({'id':json.msgTypeID,isDelete:0}).limit(1);
    
      if(empty(checkCategories)){
        return Promise.reject({
          status: "404_category_error"
        })
      }
     
      if(empty(checkMassage)){
        return Promise.reject({
          status:  "404_messagetype_error"
        })
      }
     
       var converter = JM.makeConverter(patternHelper.jmInsert());
       var jsonResult = converter(json);
       let data =  await db.knex(constants.dbUserInbox).insert(jsonResult)
       return Promise.resolve({
          status  :  "201_created",
          message : 'save appID : '+json.appID,
          resultJson : jsonResult
       })
      
  }catch(err) {
    return Promise.reject({
      status: "500_created_error"
    })
  }
};

/**
 * 
 * @param {1. update By messageID Current isRead = 1 , updatedate = now()}  json
 * @param {2. update By messageID and dataBody dataUpdate : isRead = obj.isRead , isPriority = obj.isPriority  and updatedate = now()}  json
 * @param {3. if "updateMessage" record select and return last dataUpdate } json
 */


var updateMessage  = async function (messageID,obj) {
  try{
  
   let updateMessage = null
   if(empty(obj)){
      updateMessage = await db.knex(constants.dbUserInbox).where({messageID: messageID}).update({isRead: 1,updatedate : moment().format('YYYY-MM-DD HH:mm:ss')})
   }else{
      obj.updatedate = moment().format('YYYY-MM-DD HH:mm:ss')
      updateMessage = await db.knex(constants.dbUserInbox).where({messageID: messageID}).update(obj)
   }
  

      if(updateMessage == 1)
      {
        let result = await db.knex.select(
          '*',
          db.knex.raw('DATE_FORMAT(createddate, "%Y-%m-%d %H:%i:%s") as createddate'),
         db.knex.raw('DATE_FORMAT(updatedate, "%Y-%m-%d %H:%i:%s") as updatedate')
       ).from(constants.dbUserInbox).where({'messageID':messageID}).limit(1)
       
       let converter = JM.makeConverter(patternHelper.jmSetdata())
       let jsonResult = converter(result[0])

        return Promise.resolve({
          status  :  "200_success",
          message :  "Update message ID :"+messageID,
          resultJson    :  jsonResult
        })

      }else{
        return Promise.reject({
          status : "404_put_error",
          message : messageID
        })
      }

  }catch (err){
   
      return Promise.reject({
        status : "500_db_error"
      })
  }
}

/**
 * @param {1. DELETE By messageID none condition date <= 15 set IsDelete = 1 }  json
 * @param {1.1 "dateDelete" set dateNOW - 15 day }  json
 * @param {2. DELETE AUTO add condition date <= 15 set IsDelete = 1 }  json
 */


var delMessage =  async function (messageID) {
  try{
  
    let result = null;
    if(empty(messageID)){
      let dateDelete = moment().subtract(15, 'days').format('YYYY-MM-DD')
      result = await db.knex(constants.dbUserInbox).where({isDelete:0}).whereRaw( 'date(createddate) <= ?',dateDelete).update({isDelete:1})
    }else{
      result = await db.knex(constants.dbUserInbox).where({isDelete:0}).where({messageID:messageID}).update({isDelete:1})
    }
    if(empty(result)){
      return Promise.reject({
        status:  "404_del_error",
        message: result
      })
    }else{
      return Promise.resolve({
        status  :  "204_delete"
      })
    }
  }catch (err){

    return Promise.reject({
      status:  "500_db_error"
    })
  }

}

/**
 * 
 * @param {1. ssoid = json.req.params.ssoid} json 
 * @param {2. Query by isDelete = 0 limit 1} json 
 * @param {3. Return data from json} json 
 */
var getMessageBySSOID = async function (ssoid){
  try{
    result  =   await db.knex.select('*',
    db.knex.raw('DATE_FORMAT(createddate, "%Y-%m-%d %H:%i:%s") as createddate'),
    db.knex.raw('DATE_FORMAT(updatedate, "%Y-%m-%d %H:%i:%s") as updatedate'))
    .table(constants.dbUserInbox)
    .where({ssoid:ssoid , isDelete : 0})
    .limit(1)
    if(empty(result)){
      return Promise.reject({
        status:  "404_list_error"
      })
     }else{
      let converter = JM.makeConverter(patternHelper.jmSetdata())
      let jsonResult = converter(result[0])
      return Promise.resolve({
        status     :  "200_success",
        resultJson : jsonResult
      })
     }
  }catch (err){

    return Promise.reject({
      status:  "500_db_error"
    })
  }
  

}

module.exports = {
  findCategories,
  findMessagetype,
  addMessage,
  updateMessage,
  delMessage,
  getListMessage,
  getMessageBySSOID,
  deleteTypeUserInbox,
  addTypeUserInbox,
  updateTypeUserInbox
}
