'use strict';

const version = "/v1";

module.exports = function(app) {
    app.use(version+'/userinbox',require('./controller/getCategoryType'))
    app.use(version+'/userinbox',require('./controller/addCategoryType'))
    app.use(version+'/userinbox',require('./controller/delCategoryType'))
    app.use(version+'/userinbox',require('./controller/updateCategoryType'))
    app.use(version+'/userinbox',require('./controller/getMsgType'))
    app.use(version+'/userinbox',require('./controller/delMsgType'))
    app.use(version+'/userinbox',require('./controller/addMsgType'))
    app.use(version+'/userinbox',require('./controller/updateMsgType'))
    app.use(version+'/userinbox',require('./controller/addUserInbox'))
    app.use(version+'/userinbox',require('./controller/updateUserInbox'))
    app.use(version+'/userinbox',require('./controller/delUserInbox'))
    app.use(version+'/userinbox',require('./controller/getUserInbox'))
};