/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : userinbox

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-03-21 00:19:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories_userinbox
-- ----------------------------
DROP TABLE IF EXISTS `categories_userinbox`;
CREATE TABLE `categories_userinbox` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `categories` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime DEFAULT NULL,
  `isDelete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories_userinbox
-- ----------------------------
INSERT INTO `categories_userinbox` VALUES ('1', 'abc', 'MVP PLAYER', '2018-03-09 13:53:53', '2018-03-20 17:24:20', '0');
INSERT INTO `categories_userinbox` VALUES ('2', 'Maintenance', 'Categories for MVP could be  Maintenance.', '2018-03-12 16:10:14', '2018-03-20 17:19:33', '1');
INSERT INTO `categories_userinbox` VALUES ('6', 'MVC', 'MVP PLAYER', '2018-03-20 17:42:33', '2018-03-20 17:42:33', '0');

-- ----------------------------
-- Table structure for messagetype_userinbox
-- ----------------------------
DROP TABLE IF EXISTS `messagetype_userinbox`;
CREATE TABLE `messagetype_userinbox` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `messageType` varchar(100) DEFAULT NULL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime DEFAULT NULL,
  `isDelete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of messagetype_userinbox
-- ----------------------------
INSERT INTO `messagetype_userinbox` VALUES ('1', 'Text', '2018-03-20 15:17:32', '2018-03-20 16:16:26', '1');
INSERT INTO `messagetype_userinbox` VALUES ('2', 'HTML', '2018-03-20 15:17:32', '2018-03-20 16:40:52', '1');
INSERT INTO `messagetype_userinbox` VALUES ('3', 'CODE', '2018-03-20 15:17:32', '2018-03-20 16:52:29', '0');
INSERT INTO `messagetype_userinbox` VALUES ('4', 'XML', '2018-03-20 15:17:32', '2018-03-20 16:55:27', '0');
INSERT INTO `messagetype_userinbox` VALUES ('5', 'IMAGE', '2018-03-20 15:17:32', null, '0');

-- ----------------------------
-- Table structure for message_userinbox
-- ----------------------------
DROP TABLE IF EXISTS `message_userinbox`;
CREATE TABLE `message_userinbox` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `ssoid` varchar(255) DEFAULT NULL COMMENT 'SSO ID',
  `categories_id` int(4) DEFAULT NULL COMMENT 'id type massage (Text, HTML, XML, JSON, URL)',
  `messagetype_id` int(4) DEFAULT NULL COMMENT 'massagetype_id',
  `title` varchar(255) DEFAULT NULL,
  `message` text COMMENT 'text massage (Text, HTML, XML, JSON, URL)',
  `isRead` int(1) DEFAULT '0' COMMENT 'status read massage 1 = read , 0 = unread',
  `isDelete` int(1) DEFAULT '0' COMMENT 'massage exprie status 0 = active , 1 = DELETE',
  `trueid` varchar(150) DEFAULT NULL,
  `isPriority` int(1) DEFAULT NULL,
  `updatedate` datetime DEFAULT CURRENT_TIMESTAMP,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'create massage',
  `appid` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`messageID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message_userinbox
-- ----------------------------
INSERT INTO `message_userinbox` VALUES ('1', '00090901SSO', '1', '3', 'test', '<h1>test</h1>', '1', '0', '00001911', '1', '2018-03-20 17:49:52', '2018-03-20 11:22:44', '00001800');
INSERT INTO `message_userinbox` VALUES ('2', '00090901SSO', '1', '1', 'test', '<h1>test</h1>', '1', '0', '00001911', '0', '2018-03-20 14:16:25', '2018-03-20 11:42:37', '00001800');
INSERT INTO `message_userinbox` VALUES ('3', '00090901SSO', '1', '1', 'test', '<h1>test</h1>', '0', '1', '00001911', '0', '2018-03-20 11:43:39', '2018-03-20 11:43:39', '00001800');
INSERT INTO `message_userinbox` VALUES ('4', '00090901SSO', '1', '1', 'test', '<h1>test</h1>', '0', '0', '00001911', '1', '2018-03-20 14:17:00', '2018-03-20 14:17:00', '00001800');
INSERT INTO `message_userinbox` VALUES ('5', '00090901SSO', '1', '3', 'test', '<h1>test</h1>', '0', '0', '00001911', '1', '2018-03-20 17:49:52', '2018-03-20 17:49:52', '00001800');
